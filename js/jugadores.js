const cargarJugadores = () => {
    const url = `https://www.thesportsdb.com/api/v1/json/2/searchplayers.php?p=g`;
    fetch(url)
        .then(res => res.json())
        .then(data => MostrarJugadores(data.player));
}

const MostrarJugadores = players => {
    const playersContainer = document.getElementById('players-container');
    playersContainer.innerHTML = '';
    players.forEach(player => {
        const div = document.createElement('div');
        div.classList.add('col');
        div.innerHTML = `
        <div onclick="cargarJugadorDetalles(${player.idPlayer})" class="card h-100 bg-info">
            <img src="${player.strThumb}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${player.strPlayer}</h5>
                <p class="card-text">Equipo: ${player.strTeam}</p>
                <p class="card-text">Numero Camisa: ${player.strNumber ? player.strNumber : 'Not Given'}</p>
            </div>
        </div>
        `;
        playersContainer.appendChild(div);
    })
}
cargarJugadores();

// search players
const buscarJugadores = () => {
    const searchInputField = document.getElementById('search-input-field');
    const searchValue = searchInputField.value;
    cargarJugadoresByName(searchValue);
}

const cargarJugadoresByName = name => {
    const url = `https://www.thesportsdb.com/api/v1/json/2/searchplayers.php?p=${name}`;
    fetch(url)
        .then(res => res.json())
        .then(data => MostrarJugadores(data.player));
}

// player details
const cargarJugadorDetalles = id => {
    const url = `https://www.thesportsdb.com/api/v1/json/2/lookupplayer.php?id=${id}`
    fetch(url)
        .then(res => res.json())
        .then(data => mostrarJugadorDetalles(data.players[0]));
}
const boton =() => {
    const url = `https://www.thesportsdb.com/api/v1/json/2/latestsoccer.php`
    fetch(url)
        .then(res => res.json())
        .then(data => console.log(data));
}



const mostrarJugadorDetalles = details => {
    const playerDetails = document.getElementById('player-details');
    playerDetails.innerHTML = `
    <div class="card-body">
        <img src="${details.strThumb}" class="card-img-top bg-image hover-overlay ripple" alt="" >
        <h5 class="card-title">${details.strPlayer}</h5>
        <p class="card-text"> <Strong>Nacionalidad</Strong>: ${details.strNationality}</p>
        <p class="card-text"><Strong>Deporte</Strong>: ${details.strSport}</p>
        <p class="card-text"><Strong>Equipo</Strong>: ${details.strTeam}</p>
        <p class="card-text"><Strong>Posicion</Strong>: ${details.strPosition}</p>
        <p class="card-text"><Strong>Camisa Numero</Strong>: ${details.strNumber ? details.strNumber : 'Not Given'}</p>
        <p class="card-text"><Strong>Altura</Strong>: ${details.strHeight}</p>
        <p class="card-text"><Strong>Peso</Strong>: ${details.strWeight}</p>
    </div>
    `;
}
